import React from 'react';
import './App.css';
import HolaMundo from './components/holaMundo/HolaMundo';
import List from './components/list-item/List';
import Counter from './components/counter/Counter';
import ListUser from './components/list-form/ListUser';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App-header">
  
        <nav>
          <ul>
            <li>
              <Link to="/holamundo">Hola mundo</Link>
            </li>
            <li>
              <Link to="/list">List</Link>
            </li>
            <li>
              <Link to="/listuser">List User</Link>
            </li>
            <li>
              <Link to="/counter">Counter</Link>
            </li>
          </ul>
        </nav>
        
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch className="App-header">
          <Route path="/holamundo">
            <HolaMundo />
          </Route>
          <Route path="/list">
            <List />
          </Route>
          <Route path="/listuser">
            <ListUser />
          </Route>
          <Route path="/counter">
            <Counter />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
