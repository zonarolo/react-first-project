import React, { Component } from 'react';

class Counter extends Component {

  constructor (props) {
    super(props);
    this.state = { counter: 0};
  }

  plusOne() {
    this.setState({counter: this.state.counter + 1});
  }
  minusOne() {
    this.setState({counter: this.state.counter - 1});
  }
  render () {
    return (
      <div>

        {/* El contexto de la funcion nos obliga a utilizar bind */}
        {/* <button onClick={this.plusOne.bind(this)}>+</button> */}

        <button onClick = {() => { this.plusOne()}}>+</button>
        <p>{this.state.counter}</p>
        <button onClick= {this.minusOne.bind(this)}>-</button>
      </div>
    );
  }
}

export default Counter;