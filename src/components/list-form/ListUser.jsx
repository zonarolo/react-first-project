import React, { Component } from 'react';
import UserItem from "./UserItem";
import {UserForm} from './UserForm';


class ListUser extends Component {

  constructor(props) {
    super(props)
    this.state = {
      users: [
        {
          name: "Juan Rolo",
          age: "25 años",
          rol: "Ing. Elec"
        },
        {
          name: "Juan ",
          age: "26 años",
          rol: "Estudiante"
        }
      ]
    };

  }

  render () {
    const items = [];

    for (const user of this.state.users) {
      items.push(<UserItem key={user.name} userInfo={user} />);
    }
    
    return (
      <div>
        <UserForm/>
        <ul>{items}</ul>
      </div>
    );
  }
}

export default ListUser;