import React, { Component } from 'react';

export class UserForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: ''
    }
  }


  handleChange(event) {
    console.log(event.target.name);
    this.setState({[event.target.name]: event.target.value});
  }
  render() {

    return (

      <form onSubmit={($event) => {$event.preventDefault()}}>
        <input type="text" value={this.state.name} name='name' onChange={($event) => {this.handleChange($event)}} />
        <button>Guardar</button>
      </form>
    )
    
  }
}