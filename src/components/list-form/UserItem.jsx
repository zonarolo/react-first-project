import React, { Component } from 'react';

class UserItem extends Component {

  render () {
    const user = this.props.userInfo;
    return (
      <li> { user.name} tiene {user.age} y es {user.rol}</li>
    );
  }
}

export default UserItem;