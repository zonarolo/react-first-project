import React, { Component } from 'react';
import Item from "./Item";


class List extends Component {
  render () {
    
    let items = [];
    for (let i = 0; i < 10; i++) {
      items.push(<Item key={i} id={i} index={i}/>);
    }
    
    return (
      items
    );
  }
}

export default List;